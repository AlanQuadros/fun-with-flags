import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import CountryInfo from "../views/CountryInfo.vue";

const routes = [
  {
    path: '/:region?',
    name: 'Home',
    props: true,
    component: Home
  },
  {
    path: '/country/:id',
    name: 'CountryInfo',
    props: true,
    component: CountryInfo
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
