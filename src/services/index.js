import axios from 'axios';

const api = axios.create({baseURL: 'https://restcountries.com/v2'});

export const endpoints = {
  all: '/all',
  languages: (lang) => `/lang/${lang}`,
  region: (region) => `/region/${region}`,
  name: (name) => `/name/${name}`,
  callingCode: (callingCode) => `/callingcode/${callingCode}`,
  countryByCode: (code) => `/alpha/${code}`,
}

export default api
