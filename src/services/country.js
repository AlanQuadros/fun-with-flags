import api, {endpoints} from "./index";

export async function getAllCountries() {
  const {get} = await api;
  return get(endpoints.all)
}

export async function getCountriesByLanguage(lang) {
  const {get} = await api;
  return get(endpoints.languages(lang))
}

export async function getCountriesByRegion(region) {
  const {get} = await api;
  return get(endpoints.region(region))
}

export async function getCountriesByName(name) {
  const {get} = await api;
  return get(endpoints.name(name))
}

export async function getCountriesByCallingCode(code) {
  const {get} = await api;
  return get(endpoints.callingCode(code))
}

export async function getCountriesByCode(code) {
  const {get} = await api;
  return get(endpoints.countryByCode(code))
}
